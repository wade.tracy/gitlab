export const submitPath = '_submit_path_';
export const formData = {
  firstName: 'Joe',
  lastName: 'Doe',
  companyName: 'ACME',
  companySize: '1-99',
  phoneNumber: '192919',
  country: 'US',
  state: 'CA',
};
